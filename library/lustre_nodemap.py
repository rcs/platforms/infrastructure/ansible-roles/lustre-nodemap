#!/usr/bin/python

from __future__ import print_function

DOCUMENTATION = """
---
module: lustre_nodemap.py
short_description: Configure lustre nodemaps
description:
  - Uses lctl to dynamically update lustre nodemaps live
options:
  nodemaps:
    description:
      - Dictionary/Hash containing desired nodemap config
    type: dict
requirements:
  - Uses (C(lctl)).
  - Requires PyYAML to be installed on the hosts being operated on
"""

EXAMPLES = r'''
- name: "Configure nodemaps"
  lustre_nodemap:
    activate: 1
    nodemaps:
      default:
        properties:
          deny_unknown: 1
          readonly_mount: 1
      fs_admin:
        state: present
        properties:
          trusted: 1
          admin: 1
        ranges:
        - 10.44.243.[100-200]@o2ib
      users:
        state: add
        properties:
          trusted: 1
          admin: 1
        ranges:
        - 10.44.241.[1-2]@o2ib
        fileset: "/subdir"
        idmap:
          uid:
          - 530:11000
          gid:
          - 600:11000
          projid:
          - 33:1
      old:
        state: absent
'''

RETURN = r''' # '''

from pprint import pprint
import copy
import re
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.basic import missing_required_lib

YAML_IMPORT_ERR = None
try:
    import yaml
    HAS_YAML = True
except:
    HAS_YAML = False
    YAML_IMP_ERR = traceback.format_exc()

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def boolint(val):
    '''Take a booleanish input and return an int output to match how lustre stores its values'''
    if val: return 1
    else: return 0

#There's not a perfect match between the nodemap property setting and the params returned by lctl.
#Document the mapping and allow only these.
property_param_map = {
    'admin':{'func':boolint,'param':'admin_nodemap'},
    'audit_mode':{'func':boolint,'param':'audit_mode'},
    'deny_unknown':{'func':boolint,'param':'deny_unknown'},
    'forbid_encryption':{'func':boolint,'param':'forbid_encryption'},
    'map_mode':{'func':str,'param':'map_mode'},
    'rbac':{'func':str,'param':'rbac'},
    'readonly_mount':{'func':boolint,'param':'readonly_mount'},
    'squash_gid':{'func':int,'param':'squash_gid'},
    'squash_projid':{'func':int,'param':'squash_projid'},
    'squash_uid':{'func':int,'param':'squash_uid'},
    'trusted':{'func':boolint,'param':'trusted_nodemap'},
    }

def property_param_convert(param_name):
    '''Take the name lctl param returns and convert it to the on used by nodemap_modify'''
    param_property_mismatch = {v['param']: k for k, v in property_param_map.items()}
    return param_property_mismatch[param_name]

def nid_to_parts(nid):
    ip_raw, net = nid.split('@')
    ip = ip_raw.split('.')
    return ip + [net]

def range_param_convert(param_dict):
    '''Take the values lctl param returns and stringify them to properties as though entered
using nodemap_add_range.
This makes the assumption that you can't do mid-octet splits - I've not been able to in testing.'''
    start = nid_to_parts(param_dict['start_nid'])
    end = nid_to_parts(param_dict['end_nid'])
    if start[-1] != end[-1]: raise ValueError('Non-continuous networks in nid range')
    if len(start) != len(end): raise ValueError('Differing nid range IP versions')
    for n in range(len(start)):
        if start[n] != end[n]:
            break
    if n == len(start)-1:
        return start_nid #Passed right through to the net
    else:
        #Form the range string
        ret = '.'.join(start[:n])
        nid_range = '[{}-{}]'.format(start[n],end[n])
        ret = '{}.{}'.format(ret, nid_range)
        for r in range(n,len(start)-2):
            ret = '{}.[0-255]'.format(ret)
        ret = '{}@{}'.format(ret, start[-1])
        return ret

def idmap_param_convert(param_list):
    '''Convert the lctl idmap structure to the same format as though entered in using nodemap_add_idmap.'''
    ret = {}
    for entry in param_list:
        if entry['idtype'] not in ret: ret[entry['idtype']] = []
        ret[entry['idtype']].append('{}:{}'.format(entry['client_id'],entry['fs_id']))
    return ret

def diff_sets(a, b):
    '''For a pair of set objects (or things that can be cast into sets),
Show me a set of tuples where the differing elements are placed in order that differ
between the two sets.'''
    a = set(a)
    b = set(b)
    ret_a = set()
    ret_b = set()
    for e in a.union(b):
        if e not in a:
            ret_b.add( e )
        elif e not in b:
            ret_a.add( e )
    return ret_a, ret_b

def diff_dicts(a, b):
    '''I want a dict containing the keys that differ between these two dicts.
It should contain a tuple of results from each dict, but if the values are a set-like or dict-like,
Also return the difference between these.'''
    ret_a = {}
    ret_b = {}
    for key in set(a.keys()).union(set(b.keys())):
        if key not in a:
            ret_b[key] = b[key]
        elif key not in b:
            ret_a[key] = a[key]
        elif isinstance(a[key], set) or isinstance(a[key], list) or isinstance(a[key], tuple):
            s_a, s_b = diff_sets(a[key],b[key])
            if len(s_a) > 0: ret_a[key] = s_a
            if len(s_b) > 0: ret_b[key] = s_b
        elif isinstance(a[key], dict):
            d_a, d_b = diff_dicts(a[key],b[key])
            if len(d_a.keys()) > 0: ret_a[key] = d_a
            if len(d_b.keys()) > 0: ret_b[key] = d_b
        elif a[key] != b[key]:
            ret_a[key] = a[key]
            ret_b[key] = b[key]
    return ret_a, ret_b

def handle_cmd(cmd, module, changed=False, exit_on_fail=True, run_on_check=False):
    '''Prettily handle a single cmd and wrap the fail logic nicely.'''
    if module.check_mode and run_on_check == False:
        module.warn('Not actually running: {}'.format(' '.join(cmd)))
        return '', '{} not executed.'.format(' '.join(cmd)), 0, True
    else:
        rc, out, err = module.run_command(' '.join(cmd))
        if rc != 0 and exit_on_fail:
            if module.check_mode:
                module.warn("Executing {} failed. cmd: {} rc: {}. Output: {} Error: {}".format(cmd[0],' '.join(cmd), rc, out, err))
                module.exit_json()
            else:
                module.fail_json(changed=changed, msg="Executing {} failed. cmd: {} rc: {}. Output: {} Error: {}".format(cmd[0],' '.join(cmd), rc, out, err))
    return out, err, rc, True

def check_mgs(lctl_cmd, module):
    '''Execute lctl and check if this node has a working MGS mounted.'''
    raw, _, rc, _ = handle_cmd([lctl_cmd,'device_list'], module, exit_on_fail=False, run_on_check=True)
    if 'MGS' not in raw:
        if module.check_mode:
            module.warn("No MGS found on this host.")
            module.exit_json()
        else:
            module.fail_json(changed=False, msg="No MGS found on this host.")

def parse_lctl_nodemap(lctl_cmd, module):
    '''Execute lctl and parse the nodemap output into a dict that python can handle.'''
    raw_params, _, _, _ = handle_cmd([lctl_cmd,'nodemap_info','all'], module, run_on_check=True)
    params = raw_params.strip().split('\n')
    data={}
    for param in params:
        raw_data, _, _, _ = handle_cmd([lctl_cmd,'get_param',param], module, run_on_check=True)
        if not raw_data: continue #Remove anything blank.

        split_param = param.split('.')
        #Param strings always are 'nodemap.X.Y' We just want X and Y.
        nodemap_name = split_param[1]
        if nodemap_name not in data: data[nodemap_name] = {'state':'present'}
        param_name=split_param[-1]

        raw_value = '='.join(raw_data.split('=')[1:])
        value = yaml.safe_load(raw_value)
        if param_name in ['id','exports']: #Lose the unneeded data
            continue
        elif param_name == 'sepol':
            data[nodemap_name][param_name] = value
        elif param_name == 'fileset':
            data[nodemap_name][param_name] = value
        elif param_name == 'ranges':
            data[nodemap_name][param_name]=set([range_param_convert(val) for val in value])
        elif param_name == 'idmap':
            data[nodemap_name][param_name]=idmap_param_convert(value)
        else:
            if 'properties' not in data[nodemap_name]: data[nodemap_name]['properties'] = {}
            data[nodemap_name]['properties'][property_param_convert(param_name)] = value #Convert the mismatched params into settings

    active_raw, _, _, _ = handle_cmd([lctl_cmd,'get_param','nodemap.active'], module, run_on_check=True)
    active = int(active_raw.split('=')[-1])
    return data, active

def main():
    module_args = dict(
        activate=dict(type='bool', default=None),
        nodemaps=dict(type='dict', default={}),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    result = dict(
        changed=False,
        diff={'before': '', 'after': '', 'before_header': '', 'after_header': ''}
    )

    # Check for whether we could import yaml library or not
    if not HAS_YAML:
        module.fail_json(msg=missing_required_lib("yaml"),
                         exception=YAML_IMP_ERR)

    # Store path to lctl binary
    lctl_cmd = module.get_bin_path('lctl')
    if not lctl_cmd:
        if module.check_mode:
            module.warn("'lctl' executable not found. Cannot configure routes")
            module.exit_json(**result)
        else:
            module.fail_json(changed=False, msg="'lctl' executable not found.")

    # Fail out early if there's no MGS present.
    check_mgs(lctl_cmd, module)

    # 'current' and 'wanted' are dictionaries containing the current set
    # of maps, and the wanted set of routes respectively.
    current = {}
    wanted = {}

    # First check what the current nodemap state is so we can populate the defaults
    current, current_active = parse_lctl_nodemap(lctl_cmd, module)
    module.log("Current nodemaps: \n{}".format(yaml.dump(current)))

    wanted = copy.deepcopy(current)
    wanted_active = module.params.get('activate')
    if wanted_active is None:
        wanted_active = current_active
    else:
        wanted_active = boolint(wanted_active)
    # 'wanted_nodemaps' is the argument passed into the module by the user
    # and is structured a bit differently to the yaml representation of
    # routes outputted by lctl
    wanted_nodemaps = module.params.get('nodemaps')
    if wanted_nodemaps is None:
        module.exit_json(changed=False)
    # Here we iterate over the wanted_nodemaps and convert them into the
    # format of our 'wanted' dictionary

    for name in wanted_nodemaps:
        if 'state' in wanted_nodemaps[name] and wanted_nodemaps[name]['state'] == 'absent':
            if name in wanted:
                del wanted[name]
            continue
        if name not in wanted:
            wanted[name] = {'state':'present'}
        if 'properties' in wanted_nodemaps[name]:
            if any(key not in property_param_map for key in wanted_nodemaps[name]['properties']):
                module.fail_json(changed=False, msg="One or more properties present in nodemap that are not valid: {}".format(wanted_nodemaps[name]['properties']))
            if 'properties' not in wanted[name]:
                wanted[name]['properties'] = {}
            for prop in wanted_nodemaps[name]['properties']:
                # Use the appropriate function to transform the input data into the correct type lctl will want.
                wanted[name]['properties'][prop] = property_param_map[prop]['func'](wanted_nodemaps[name]['properties'][prop])
        if 'fileset' in wanted_nodemaps[name]:
            if not isinstance(wanted_nodemaps[name]['fileset'], str):
                module.fail_json(changed=False, msg="Can only accept one fileset per nodemap.")
            wanted[name]['fileset'] = wanted_nodemaps[name]['fileset']
        if 'ranges' in wanted_nodemaps[name]:
            if 'ranges' not in wanted[name]:
                wanted[name]['ranges'] = set()
            if isinstance(wanted_nodemaps[name]['ranges'], str):
                wanted_ranges = set([ wanted_nodemaps[name]['ranges'] ])
            elif not isinstance(wanted_nodemaps[name]['ranges'], list):
                module.fail_json(changed=False, msg="Nodemap ranges must be a list of strings.")
            else:
                wanted_ranges = set(wanted_nodemaps[name]['ranges'])
            if 'state' in wanted_nodemaps[name] and wanted_nodemaps[name]['state'] == 'add':
                wanted[name]['ranges'] = wanted[name]['ranges'].union(wanted_ranges)
            else:
                wanted[name]['ranges'] = set(wanted_nodemaps[name]['ranges'])
        if 'idmap' in wanted_nodemaps[name]:
            if 'idmap' not in wanted[name]:
                wanted[name]['idmap'] = {}
            if 'state' in wanted_nodemaps[name] and wanted_nodemaps[name]['state'] == 'add':
                wanted[name]['idmap'].update(wanted_nodemaps[name]['idmap'])
            else:
                wanted[name]['idmap'] = wanted_nodemaps[name]['idmap']
        if 'sepol' in wanted_nodemaps[name]:
            wanted[name]['sepol'] = wanted_nodemaps[name]['sepol']

    module.log("Wanted nodemaps: \n{}".format(yaml.dump(wanted)))

    excess, missing = diff_dicts(current, wanted)

    module.log("Missing nodemaps: \n{}".format(yaml.dump(missing, sort_keys=True)))

    result['diff']['before'] = {**current,'activate':current_active}
    result['diff']['after'] = {**wanted,'activate':wanted_active}

    ##############
    # Making changes
    #
    # Non-stop section with changes to apply. Don't die until the end in case of failure.
    # Use 'results' to store output of commands run as feedback to user
    result['results'] = []
    failed_commands = []

    for map in missing:
        if map not in current:
            cmd = [lctl_cmd,'nodemap_add',map]
            out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
            if rc != 0:
                failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
            result['results'].extend([' '.join(cmd)])
        if 'properties' in missing[map]:
            for property in missing[map]['properties']:
                cmd = [lctl_cmd,'nodemap_modify', '--name', map, '--property', property, '--value', str(missing[map]['properties'][property])]
                out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
                if rc != 0:
                    failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
                result['results'].extend([' '.join(cmd)])
        if 'ranges' in missing[map]:
            for range in missing[map]['ranges']:
                cmd = [lctl_cmd,'nodemap_add_range', '--name', map, '--range', range]
                out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
                if rc != 0:
                    failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
                result['results'].extend([' '.join(cmd)])
        if 'sepol' in missing[map]:
            cmd = [lctl_cmd,'nodemap_set_sepol', '--name', map, '--sepol', missing[map]['sepol']]
            out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
            if rc != 0:
                failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
            result['results'].extend([' '.join(cmd)])
        if 'fileset' in missing[map]:
            cmd = [lctl_cmd,'nodemap_set_fileset', '--name', map, '--fileset', missing[map]['fileset']]
            out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
            if rc != 0:
                failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
            result['results'].extend([' '.join(cmd)])
        if 'idmap' in missing[map]:
            for type in missing[map]['idmap']:
                for idmap in missing[map]['idmap'][type]:
                    cmd = [lctl_cmd,'nodemap_add_idmap', '--name', map, '--idtype', type, '--idmap', idmap]
                    out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
                    if rc != 0:
                        failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
                    result['results'].extend([' '.join(cmd)])

    for map in current:
        if map not in wanted:
            cmd = [lctl_cmd,'nodemap_del',map]
            out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
            if rc != 0:
                failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
            result['results'].extend([' '.join(cmd)])

    if current_active != wanted_active:
        cmd = [lctl_cmd,'nodemap_activate',str(wanted_active)]
        out, err, rc, result['changed'] = handle_cmd(cmd, module, result['changed'], exit_on_fail=False)
        if rc != 0:
            module.log('{} : {}'.format(out, err))
            failed_commands.append({'command': ' '.join(cmd), 'rc': rc, 'err': err})
        result['results'].extend([' '.join(cmd)])

    # If any commands failed to run, exit with error and add this to result output
    if failed_commands:
        result['failed_commands'] = failed_commands
        module.fail_json(msg="{} lctl commands failed to execute successfully.".format(len(failed_commands)), **result)

    current, current_active = parse_lctl_nodemap(lctl_cmd, module)
    result['nodemaps'] = current
    result['active'] = current_active

    # Success
    module.exit_json(**result)

if __name__ == '__main__':
    main()
