Lustre Nodemap
==============

Configure lustre nodemaps on a Lustre MGS by using lctl to dynamically update
them as per the applied nodemap var.

Also takes an 'activate' var to control activating nodemaps altogether.

Updated as of Lustre 2.16.

Examples
--------

```
- name: "Update nodemaps"
  hosts: all
  gather_facts: false
  become: true
  any_errors_fatal: false
  roles:
  - role: "lustre_nodemap"
    activate: 1
    nodemaps:

# default is special - you can't delete it and it has limited options.
# See the Lustre Manual for details.
      default:
        properties:
          deny_unknown: 1

# Don't forget that you need to set servers to admin and trusted, otherwise
# The MDSes and OSSes can't talk to each other properly!
      servers:
        properties:
          admin: 1
          trusted: 1
        ranges:
        - 192.168.10.[10-14]@tcp
        - 192.168.10.[101-102]@tcp
      clients:
        properties:
          trusted: 1
        ranges:
        - 192.168.10.[200-210]@tcp
      special_clients:
        fileset: "/subdir"
        properties:
          trusted: 1
        ranges:
        - 192.168.10.[220]@tcp
        idmaps:
          uid:
          - 530:11000
          gid:
          - 600:11000
          projid:
          - 33:1
      fs_admin:
        properties:
          trusted: 1
          admin: 1
        ranges:
        - 192.168.10.[240-250]@tcp

# state: add an additional range or idmap to an already existing nodemap
# without needing to rewrite it fully.
      update_clients:
        state: add
        ranges:
        - 192.168.10.[224]@tcp

# state: absent will remove a nodemap entirely.
      old:
        state: absent
```
